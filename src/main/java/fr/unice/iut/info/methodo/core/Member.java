package fr.unice.iut.info.methodo.core;

import java.util.UUID;

/**
 * Created by blay on 20/08/2016.
 * Support definition of a member.
 */


public class Member {

    private String id;
    private String name;
    private String firstname;


    public Member(String name, String firstname) {
        this(UUID.randomUUID().toString(), name, firstname);
    }

    public Member(String id, String name, String firstname) {
        this.name = name;
        this.firstname = firstname;
        this.id = id;
    }



    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;

        Member member = (Member) o;

        if (id != null ? !id.equals(member.id) : member.id != null) return false;
        if (name != null ? !name.equals(member.name) : member.name != null) return false;
        return firstname != null ? firstname.equals(member.firstname) : member.firstname == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }
}
