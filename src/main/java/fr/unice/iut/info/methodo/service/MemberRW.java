package fr.unice.iut.info.methodo.service;

import fr.unice.iut.info.methodo.core.Member;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by blay on 20/08/2016.
 * Based on Crunchify.com
 *
 * Support serialisation of a Member in Json format
 */

public class MemberRW {

    private final static Logger LOGGER = Logger.getLogger(MemberRW.class.getName());
    private static FileHandler fh;

    static {
        try {
            fh = new FileHandler("MemberRWLog.txt");
            LOGGER.addHandler(fh);
        } catch (IOException e) {
           System.out.println("Log file for MemberRW not created");
        }
    }


    private JSONParser parser = new JSONParser();

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String FIRSTNAME = "firstname";


    private String firstname(JSONObject jsonObject) {
        return (String) jsonObject.get(FIRSTNAME);
    }

    private String name(JSONObject jsonObject) {
        return (String) jsonObject.get(NAME);
    }

    private String id(JSONObject jsonObject) {
        return (String) jsonObject.get(ID);
    }

    public void writeMember(Member member, BufferedWriter fileWriter) throws IllegalAccessException, IOException {
        JSONObject obj = getJsonObject(member);

        fileWriter.write(obj.toJSONString());
        fileWriter.newLine();
        // Fine : the lowest priority,
        LOGGER.fine("Successfully Copied JSON Object to File...");
        LOGGER.log(Level.INFO,"\nJSON Object: " + obj);
    }

    public JSONObject getJsonObject(Member member) {
        JSONObject obj = new JSONObject();
        obj.put(ID, member.getId());
        obj.put(NAME, member.getName());
        obj.put(FIRSTNAME, member.getFirstname());
        return obj;
    }


    public Member readMember(BufferedReader fileReader) throws IOException, ParseException {
        Object obj = parser.parse(fileReader);

        return getMember((JSONObject) obj);
    }

    public Member getMember(JSONObject obj) {
        JSONObject jsonObject = obj;
        String name = (String) jsonObject.get(NAME);
        String firstName = (String) jsonObject.get(FIRSTNAME);
        String id = (String) jsonObject.get(ID);

        return new Member(id, name, firstName);
    }
}
