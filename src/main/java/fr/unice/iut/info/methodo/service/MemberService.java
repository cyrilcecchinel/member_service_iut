package fr.unice.iut.info.methodo.service;

import fr.unice.iut.info.methodo.core.Member;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Created by blay on 20/08/2016.
 * Modified by blay on 20/08/2016.
 */
public class MemberService {

    private final static Logger LOGGER = Logger.getLogger(MemberService.class.getName());
    private static FileHandler fh;

    private String fileName;


    private  HashMap<String, Member> memberIdMap = new HashMap<String, Member>();
    private MemberSetRW memberSetRW = new MemberSetRW();

    public MemberService(String fileName) throws IOException, ParseException {
        this.fileName = fileName;
        try {
            ArrayList<Member> memberList = memberSetRW.readMembers(new BufferedReader(new FileReader(fileName)));
            for (Member member:memberList) {
                memberIdMap.put(member.getId(),member);
            }
        }
        catch (FileNotFoundException e){
            LOGGER.log(Level.WARNING,"File exception : " + e);
        }
    }

    public void saveMembers(String fileName) throws IOException, IllegalAccessException {
        this.fileName = fileName;
        //ecriture des membres dans le fichier
        LOGGER.log(Level.INFO,"Sauvegarde des membres : " + memberIdMap.values());
        LOGGER.log(Level.INFO,"dans : " + fileName);
        memberSetRW.writeMembers(
                memberIdMap.values(),
                new BufferedWriter(new FileWriter(fileName))
        );
    }


    public List<Member> getAllMembers() {
        return new ArrayList<Member>(memberIdMap.values());
    }

    public Member getMember(String id)
    {
        return memberIdMap.get(id);
    }


    /**
     * @param name : name of wanted members
     * @return a list of members whose name is {@code name}
     */
    public Collection<Member> getMembers(String name)
    {
        Collection<Member> members = memberIdMap.values();
        ArrayList<Member> correspondingMembers = new ArrayList<Member>();
        for (Member m : members)
        {
            if (m.getName().equals(name))
                correspondingMembers.add(m);
        }
        return correspondingMembers;
    }

    public Member addMember(Member member)
    {
        memberIdMap.put(member.getId(), member);
        return member;
    }

    public Member updateMember(Member member)
    {   //if the member is unknown, it is added
        memberIdMap.put(member.getId(), member);
        return member;
    }


    public void deleteMember(String id)
    {
        memberIdMap.remove(id);
    }

    private HashMap<String, Member> getMemberIdMap(){
        return memberIdMap;
    }


}
