package fr.unice.iut.info.methodo.service;

import fr.unice.iut.info.methodo.core.Member;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Created by blay on 20/08/2016.
 */
public class MemberSetRW {

    private final static Logger LOGGER = Logger.getLogger(MemberSetRW.class.getName());
    private static FileHandler fh;


    private JSONParser parser = new JSONParser();
    private MemberRW memberReaderWriter = new MemberRW();

    @SuppressWarnings("unchecked")
    public void writeMembers(Collection<Member> members, BufferedWriter fileWriter) throws IllegalAccessException, IOException {
        LOGGER.info("appel à writeMembers avec les objets : " + members);
        JSONObject obj = getJsonObject(members);

        fileWriter.write(obj.toJSONString());
        fileWriter.newLine();
        fileWriter.close();
        LOGGER.info("Successfully Copied JSON Objects to File...");
        LOGGER.info("\nJSON Object: " + obj);
    }

    public JSONObject getJsonObject(Collection<Member> members) {
        JSONObject obj = new JSONObject();

        JSONArray memberListInJSON = new JSONArray();

        for (Member m : members) {
            memberListInJSON.add(memberReaderWriter.getJsonObject(m));
        }

        obj.put("members", memberListInJSON);
        LOGGER.info("Resultat de getJsonObject : " + obj);
        return obj;
    }


    public ArrayList<Member>  readMembers(BufferedReader fileReader) throws IOException, ParseException {
            Object obj = parser.parse(fileReader);

            JSONObject jsonObject = (JSONObject) obj;

            JSONArray membersInJSON = (JSONArray) jsonObject.get("members");
            ArrayList<Member> memberSet = new ArrayList<Member>(membersInJSON.size());
            for ( Object m : membersInJSON){
                memberSet.add(memberReaderWriter.getMember((JSONObject) m));
            }
            return memberSet;
    }

}
