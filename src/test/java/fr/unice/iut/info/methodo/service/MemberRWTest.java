package fr.unice.iut.info.methodo.service;


import fr.unice.iut.info.methodo.core.Member;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by blay on 20/08/2016.
 */
public class MemberRWTest {

    MemberRW ms = new MemberRW();
    BufferedWriter fileWriter;

    Member one = new Member("myName", "myFirstName");
    Member two = new Member("myName2", "myFirstName2");
    Member three = new Member("myName3", "myFirstName3");

    String fileName = "testFile";

    public MemberRWTest() throws IOException {
    }

    @org.junit.Before
    public void setUp() throws Exception {
        one = new Member("001", "myName", "myFirstName");
        two = new Member("myName2", "myFirstName2");
        three = new Member("myName3", "myFirstName3");

        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
        ms.writeMember(one, fileWriter);
        fileWriter.close();
    }





    @org.junit.Test
    public void writeMember() throws Exception {
        FileReader fr = new FileReader (fileName);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        br.close();
        assertEquals("Meme string " + line ,
                "{\"firstname\":\"myFirstName\",\"name\":\"myName\",\"id\":\"001\"}" ,
                line );
    }

    @org.junit.Test
    public void readOneMember() throws Exception {
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter("testFile"));
        ms.writeMember(one, fileWriter);
        fileWriter.close();
        Member m =  ms.readMember(new BufferedReader(new FileReader("testFile")) ) ;
        assertEquals("objet bien défini",one,m);
    }

    @org.junit.After
    public void tearDown() {
        File file = new File(fileName);
        file.delete();
    }

}