package fr.unice.iut.info.methodo.service;

import fr.unice.iut.info.methodo.core.Member;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by blay on 21/08/2016.
 */

public class MemberServiceTest {


    Member one = new Member("myName", "myFirstName");
    Member two = new Member("myName2", "myFirstName2");
    Member three = new Member("myName3", "myFirstName3");

    List<Member> members;

    String fileName = "testFile";
    MemberService ms ;

    public MemberServiceTest() throws IOException, ParseException {
    }

    @org.junit.Before
    public void setUp() throws Exception {
        System.out.println(members);
        one = new Member("001", "myName", "myFirstName");
        two = new Member("myName2", "myFirstName2");
        three = new Member("myName3", "myFirstName3");
        members = Arrays.asList(one,two,three);
        System.out.println(members);
        //To be sure there is no member when beginning tests
        File f = new File(fileName);
        f.delete();
        ms= new MemberService(fileName);
    }

    public void setUpForMembers() throws Exception {
        ms.addMember(one);
        ms.addMember(two);
        ms.addMember(three);
    }

    @Test
    public void readMembersWhenCreatingAService() throws Exception {
        setUpForMembers();
        ms.saveMembers(fileName);
        ms= new MemberService(fileName);
        assertEquals("3 membres connus du service",3,ms.getAllMembers().size());
    }


    @Test
    public void saveMembers() throws Exception {
        setUpForMembers();
        ms.saveMembers("autre");
        FileReader fr = new FileReader ("autre");
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        System.out.println("Dans le fichier, je lis : " + line);
        br.close();
        assertTrue(line.contains("{\"firstname\":\"myFirstName\",\"name\":\"myName\",\"id\":\"001\"}"));
        assertTrue(line.endsWith("}]}"));
    }

    @Test
    public void getAllMembers() throws Exception {
        setUpForMembers();
        assertEquals("liste de même taille", 3,ms.getAllMembers().size());
//        assertEquals("liste bien construite", members,ms.getAllMembers());

    }

    @Test
    public void getMember() throws Exception {
        ms.addMember(one);
        ms.addMember(two);
        assertEquals("membre présent dans la liste", one, ms.getMember("001"));
        assertEquals("membre présent dans la liste", two, ms.getMember(two.getId()));
    }

    @Test
    public void getMembers() throws Exception {
        ms.addMember(one);
        ms.addMember(two);
        ms.addMember(new Member("myName2", "myFirstName2"));
        Collection<Member> oneMember = ms.getMembers("myName");
        Collection<Member> twoMembers = ms.getMembers("myName2");
        assertEquals("un membre présent dans la liste", 1, oneMember.size());
        assertEquals("deux membres présents dans la liste", 2, twoMembers.size());
        assertEquals("aucun membre présent dans la liste", 0, ms.getMembers("name").size());
    }

    @Test
    public void checkInitEmptyService() throws Exception {
        assertEquals("aucun membre dans la liste", 0, ms.getAllMembers().size());

    }

    @Test
    public void addOneMember() throws Exception {
        ms.addMember(one);
        assertEquals("un membre dans la liste", 1, ms.getAllMembers().size());
    }


    @Test
    public void addTwoMember() throws Exception {
        ms.addMember(one);
        ms.addMember(two);
        assertEquals("2 membres dans la liste", 2, ms.getAllMembers().size());
    }


    @Test
    public void updateMember() throws Exception {
        ms.addMember(one);
        one.setFirstname("firstname");
        ms.updateMember(one);
        Member m =  ms.getMember(one.getId());
        assertEquals("membre modifiée", "firstname", m.getFirstname());
    }

    @Test
    public void deleteMember() throws Exception {
        ms.addMember(one);
        ms.deleteMember(one.getId());
        assertNull(ms.getMember(one.getId()));
    }

    @Test
    public void getMemberIdMap() throws Exception {

    }

    @org.junit.After
    public void tearDown() {
        File file = new File(fileName);
        File file2 = new File("autre");
        file.delete();
        file2.delete();
    }
}