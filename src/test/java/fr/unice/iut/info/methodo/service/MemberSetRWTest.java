package fr.unice.iut.info.methodo.service;

import fr.unice.iut.info.methodo.core.Member;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by blay on 20/08/2016.
 */
public class MemberSetRWTest {
    MemberSetRW ms = new MemberSetRW();
    BufferedWriter fileWriter;

    Member one = new Member("myName", "myFirstName");
    Member two = new Member("myName2", "myFirstName2");
    Member three = new Member("myName3", "myFirstName3");

    List<Member> members;

    String fileName = "testFile";

    @org.junit.Before
    public void setUp() throws Exception {
        one = new Member("001", "myName", "myFirstName");
        two = new Member("myName2", "myFirstName2");
        three = new Member("myName3", "myFirstName3");
        members = Arrays.asList(one,two,three);
    }



    public void init() throws Exception {
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
        ms.writeMembers(members, fileWriter);
        fileWriter.close();
    }

    @Test
    public void writeMembers() throws Exception {
        init();
        FileReader fr = new FileReader (fileName);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        br.close();
        assertTrue(line.startsWith("{\"members\":[{\"firstname\":\"myFirstName\",\"name\":\"myName\",\"id\":\"001\"},{\"firstname\":\"myFirstName2\","));
        assertTrue(line.endsWith("}]}"));
    }


    @Test
    public void getJsonObject() throws Exception {
        JSONObject objectJSON = ms.getJsonObject(members);
        System.out.println(objectJSON);
    }

    @Test
    public void readMembers() throws Exception {
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter("testFile"));
        ms.writeMembers(members, fileWriter);
        fileWriter.close();
        ArrayList<Member> m =  ms.readMembers(new BufferedReader(new FileReader("testFile")) ) ;
        assertEquals("liste bien définie" + m,members,m);
    }

    @org.junit.After
    public void tearDown() {
        File file = new File(fileName);
        file.delete();
    }

}